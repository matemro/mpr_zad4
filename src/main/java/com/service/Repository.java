package com.service;
import java.util.List;

public interface Repository<TEntity> {
    public List<TEntity> get();
    public void clear();
}
