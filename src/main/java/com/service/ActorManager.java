package com.service;

import com.domain.Actor;
import com.mappers.ResultSetMapper;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ActorManager extends RepositoryBase {
    private String createTable = "CREATE TABLE IF NOT EXISTS Actor(id int primary key auto_increment, name varchar(20), dateOfBith datetime)";

    public ActorManager(ResultSetMapper<Actor> mapper) {
        super(mapper);
    }

    public int add(Actor actor) {
        int count = 0;
        try {
            addSql.setString(1, actor.getName());
            addSql.setDate(2, Date.valueOf(actor.getDateOfBirth()));

            count = addSql.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }



    protected String addSqlStmt() {
        return "INSERT INTO Actor (name, dateOfBirth) VALUES (?, ?)";
    }

    protected String deleteSqlStmt() {
        return "DELETE FROM Actor";
    }

    protected String getSqlStmt() {
        return "SELECT id, name, dateOfBirth FROM Actor";
    }

    protected String type() {
        return "Actor";
    }
}
