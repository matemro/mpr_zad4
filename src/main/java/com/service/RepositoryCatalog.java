package com.service;


import com.domain.Actor;
import com.mappers.ActorResultMapper;
import com.uow.UnityOfWork;

public class RepositoryCatalog implements IRepositoryCatalog {
    UnityOfWork uow;
    public Repository<Actor> actorRepository(){
        this.uow = uow;

        return new ActorManager(new ActorResultMapper());
    }
    public void saveChanges() {
        uow.saveChanges();
    }
}
