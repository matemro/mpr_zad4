package com.service;

import com.domain.Actor;


public interface IRepositoryCatalog {
    Repository<Actor> actorRepository();

    void saveChanges();
}
