package com.service;

import com.domain.TvSeries;
import com.mappers.ResultSetMapper;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TvSeriesManager extends RepositoryBase {
    SeasonManager seasonManager;
    private String createTable = "CREATE TABLE IF NOT EXISTS TvSeries(id int primary key auto_increment, name varchar(20), seasons enum)";

    public TvSeriesManager (ResultSetMapper<TvSeries> mapper) {
        super(mapper);
        seasonManager = new SeasonManager();
    }

    public int add(TvSeries tvSeries) {
        int count = 0;
        try {
            addSql.setString(1, tvSeries.getName());
            addSql.setArray(2, tvSeries.getSeasons());

            count = addSql.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    public List<TvSeries> getAllActors() {
        List<TvSeries> tvSeriess = new ArrayList<TvSeries>();

        try {
            ResultSet rs = getSql.executeQuery();

            while (rs.next()) {

                tvSeriess.add(p);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tvSeriess;
    }

    protected String addSqlStmt() {
        return "INSERT INTO TvSeries (name, seasons) VALUES (?, ?)";
    }

    protected String deleteSqlStmt() {
        return "DELETE FROM TvSeries";
    }

    protected String getSqlStmt() {
        return "SELECT id, name, seasons FROM TvSeries";
    }

    protected String type() {
        return "TvSeries";
    }
}
