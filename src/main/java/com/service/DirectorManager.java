package com.service;

import com.domain.Director;
import com.mappers.ResultSetMapper;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DirectorManager extends RepositoryBase {
    private String createTable = "CREATE TABLE IF NOT EXISTS Director(id int primary key auto_increment, name varchar(20), dateOfBith datetime, biography text)";

    public DirectorManager(ResultSetMapper<Director> mapper) {
        super(mapper);
    }

    public int add(Director director) {
        int count = 0;
        try {
            addSql.setString(1, director.getName());
            addSql.setDate(2, Date.valueOf(director.getDateOfBirth()));
            addSql.setString(3, director.getBiography());
            count = addSql.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    public List<Director> get() {
        List<Director> directors = new ArrayList<Director>();

        try {
            ResultSet rs = getSql.executeQuery();

            while (rs.next()) {

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return directors;
    }

    protected String addSqlStmt() {
        return "INSERT INTO Director (name, dateOfBirth, biography) VALUES (?, ?)";
    }

    protected String deleteSqlStmt() {
        return "DELETE FROM Director";
    }

    protected String getSqlStmt() {
        return "SELECT id, name, dateOfBirth, biography FROM Director";
    }

    protected String type() {
        return "Director";
    }
}
