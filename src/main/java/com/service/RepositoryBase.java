package com.service;
import com.mappers.ResultSetMapper;
import java.util.List;
import java.sql.*;

import com.uow.UnityOfWork;
import com.uow.UnityOfWorkRepository;

public abstract class RepositoryBase<Entity> implements Repository<Entity>, UnityOfWorkRepository {
    protected Connection connection;
    protected String url = "jdbc:hsqldb:hsql://localhost/workdb";
    protected PreparedStatement addSql;
    protected PreparedStatement deleteSql;
    protected PreparedStatement getSql;
    protected Statement statement;
    protected String createTable;
    ResultSetMapper<Entity> mapper;
    UnityOfWork uow;

    protected RepositoryBase(ResultSetMapper<Entity> mapper, UnityOfWork uow)  {
        this.mapper = mapper;
        this.uow = uow;
        try {
            connection = DriverManager.getConnection(url);
            statement = connection.createStatement();

            ResultSet rs = connection.getMetaData().getTables(null, null, null,
                    null);
            boolean tableExists = false;
            while (rs.next()) {
                if (type().equalsIgnoreCase(rs.getString("TABLE_NAME"))) {
                    tableExists = true;
                    break;
                }
            }

            if (!tableExists)
                statement.executeUpdate(createTable);

            addSql = connection
                    .prepareStatement(addSqlStmt());
            deleteSql = connection
                    .prepareStatement(deleteSqlStmt());
            getSql = connection
                    .prepareStatement(getSqlStmt());

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    protected Connection getConnection() {
        return connection;
    }
    public void clear() {
        try {
            deleteSql.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public List<Entity> get(){
        List<Entity> result = new ArrayList<Entity>();
        try {
            ResultSet rs = getSql.executeQuery();
            while (rs.next()) {
                result.add(mapper.map(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public void add(Entity entity) {
        Entity ent = new Entity();
        ent.setEntity(entity);
        uow.markAsNew(ent);
    }
    public void delete(Entity entity) {
        Entity ent = new Entity();
        ent.setEntity(entity);
        uow.markAsDeleted(ent);
    }
    public void update(Entity entity) {
        Entity ent = new Entity();
        ent.setEntity(entity);
        uow.markAsChanged(ent);
    }

    protected abstract String addSqlStmt();

    protected abstract String deleteSqlStmt();

    protected abstract String getSqlStmt();

    protected abstract String type();
}
