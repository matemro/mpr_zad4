package com.service;

import com.domain.Episode;
import com.domain.Season;
import com.mappers.ResultSetMapper;
import java.sql.*;
import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class EpisodeManager extends RepositoryBase {
    private String createTable = "CREATE TABLE IF NOT EXISTS Episode(id int(11) primary key auto_increment, season_id int(11), name varchar(20), releaseDate datetime, episodeNumber int, duration int)";

    public EpisodeManager(ResultSetMapper<Episode> mapper) {
        super(mapper);
    }

    public int add(Episode episode, Season season) {
        int count = 0;
        try {
            addSql.setInt(2, season.getId());
            addSql.setString(1, episode.getName());
            addSql.setDate(3, Date.valueOf(episode.getReleaseDate()));
            addSql.setInt(4, episode.getEpisodeNumber());
            addSql.setTime(5, Time.valueOf(LocalTime.MIDNIGHT.plus(episode.getDuration())));
            count = addSql.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    public List<Episode> get() {
        List<Episode> episodes = new ArrayList<Episode>();

        try {
            ResultSet rs = getSql.executeQuery();

            while (rs.next()) {
                episodes.add(p);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return episodes;
    }

    protected String addSqlStmt() {
        return "INSERT INTO Episode (name, season_id, releaseDate, episodeNumber, duration) VALUES (?, ?)";
    }

    protected String deleteSqlStmt() {
        return "DELETE FROM Episode";
    }

    protected String getSqlStmt() {
        return "SELECT id, name, episodeNumber, duration FROM Episode";
    }

    protected String type() {
        return "Episode";
    }
}
