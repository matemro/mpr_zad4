package com.uow;

public enum EntityState {
    New, Deleted, Changed, UnChanged, Unknown
}
