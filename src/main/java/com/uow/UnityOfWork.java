package com.uow;

import javax.swing.text.html.parser.Entity;

public interface UnityOfWork {
    public void markAsNew(Entity entity);
    public void markAsDeleted(Entity entity);
    public void markAsChanged(Entity entity);
    public void saveChanges();
    public void rollback();
}
