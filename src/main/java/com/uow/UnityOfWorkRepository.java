package com.uow;

import javax.swing.text.html.parser.Entity;

public interface UnityOfWorkRepository {
    public void persistAdd(Entity entity);
    public void persistUpdate(Entity entity);
    public void persistDelete(Entity entity);
}
