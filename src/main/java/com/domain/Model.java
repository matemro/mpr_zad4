package com.domain;

/**
 * Created by Uzytkownik on 2017-12-17.
 */
public abstract class Model {
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
