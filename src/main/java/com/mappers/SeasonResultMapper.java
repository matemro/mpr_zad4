package com.mappers;
import com.domain.Season;

import java.sql.ResultSet;
import java.sql.SQLException;
public class SeasonResultMapper  implements ResultSetMapper<Season>{

    public Season map(ResultSet rs) throws SQLException {
        Season p = new Season();
        p.setId(rs.getInt("id"));
        p.setSeasonNumber(rs.getInt("seasonNumber"));
        p.setYearOfRelease(rs.getInt("yearOfRelease"));
        p.setEpisodes(episodeManager.get());
        return p;
    }
}
