package com.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.domain.Actor;

public class ActorResultMapper  implements ResultSetMapper<Actor>{

    public Actor map(ResultSet rs) throws SQLException {
        Actor p = new Actor();
        p.setId(rs.getInt("id"));
        p.setName(rs.getString("name"));
        p.setDateOfBirth(rs.getDate("dateOfBirth").toLocalDate());
        return p;
    }
}

