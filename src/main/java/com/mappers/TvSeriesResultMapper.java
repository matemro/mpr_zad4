package com.mappers;
import com.domain.TvSeries;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TvSeriesResultMapper  implements ResultSetMapper<TvSeries>{

    public TvSeries map(ResultSet rs) throws SQLException {
        TvSeries p = new TvSeries();
        p.setId(rs.getInt("id"));
        p.setName(rs.getString("name"));
        p.setSeasons(seasonManager.get());
        return p;
    }
}
